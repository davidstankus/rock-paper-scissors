/**
 * Created by davidstankus on 9/24/15.
 */

function play(userChoice) {
    var computerChoice = Math.random();
    if (computerChoice < 0.34) {
        computerChoice = "Rock";
    } else if(computerChoice <= 0.67) {
        computerChoice = "Paper";
    } else {
        computerChoice = "Scissors";
    }

    document.getElementById('userResult').innerHTML = ("User chose:" + ' ' + userChoice);
    document.getElementById('computerResult').innerHTML = ("Computer chose:" + ' ' + computerChoice);
    document.getElementById('finalResult').innerHTML = (compare(userChoice, computerChoice));
}

var compare = function(choice1, choice2) {
    if (choice1 === choice2) {
        return "The result is a tie!"
    }
    else if (choice1 === "Rock") {
        if (choice2 === "Scissors") {
            return "You win!"
        } else {
            return "Computer wins"
        }
    }
    else if (choice1 === "Paper") {
        if (choice2 === "Rock") {
            return "You win!"
        } else {
            return "Computer wins"
        }
    }
    else if (choice1 === "Scissors") {
        if (choice2 === "Rock"){
            return "Computer wins"
        } else {
            return "You win!"
        }
    }
}

